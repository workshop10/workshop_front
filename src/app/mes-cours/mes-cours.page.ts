import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mes-cours',
  templateUrl: 'mes-cours.page.html',
  styleUrls: ['mes-cours.page.scss']
})
export class MesCoursPage implements OnInit {
  private sub: any;
  activeRoute: string;

  constructor(private router: Router) {}

  ngOnInit() {
    console.log(this.router.url);
  }

  goToCoursDetail(cours: string) {
    this.router.navigate([this.router.url + '/detail', cours]);
  }
}
