import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { MesCoursPage } from './mes-cours.page';

describe('MesCoursPage', () => {
  let component: MesCoursPage;
  let fixture: ComponentFixture<MesCoursPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MesCoursPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(MesCoursPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
