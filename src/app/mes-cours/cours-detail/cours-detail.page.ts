import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cours-detail',
  templateUrl: 'cours-detail.page.html',
  styleUrls: ['cours-detail.page.scss']
})
export class CoursDetailPage implements OnInit {
  cours: string;
  private sub: any;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.cours = params.cours;
    });
  }
}
