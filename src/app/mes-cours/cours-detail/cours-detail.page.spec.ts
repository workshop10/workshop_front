import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { CoursDetailPage } from './cours-detail.page';

describe('CoursDetailPage', () => {
  let component: CoursDetailPage;
  let fixture: ComponentFixture<CoursDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CoursDetailPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CoursDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
