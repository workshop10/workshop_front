
/**
 * Chatter - Chat themes Ionic 4 (https://www.enappd.com)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source .
 *
 */


import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, IonContent } from '@ionic/angular';

@Component({
  selector: 'app-tchat',
  templateUrl: './tchat.page.html',
  styleUrls: ['./tchat.page.scss'],
})
export class TchatPage implements OnInit {
  @ViewChild('content', { static: true }) content: IonContent;
  @ViewChild('chat_input', { static: true }) messageInput: ElementRef;
  User = 'Me';
  toUser = 'Tutor';
  inpText: any;
  editorMsg = '';
  showEmojiPicker = false;
  msgList: Array<{
    userId: any,
    userName: any,
    userAvatar: '../../../../assets/icon/favicon.png',
    time: any,
    message: any,
    upertext: any,
    status: any;
  }>;
  public count = 0;
  public arr = [
    {
      messageId: '1',
      userId: '140000198202211138',
      userName: 'Luff',
      userImgUrl: './assets/user.jpg',
      toUserId: '210000198410281948',
      toUserName: 'Hancock',
      userAvatar: '../../../../assets/icon/favicon.png',
      time: 1488349800000,
      message: 'Hey, that\'s an awesome chat UI',
      status: 'success'

    },
    {
      messageId: '2',
      userId: '210000198410281948',
      userName: 'Hancock',
      userImgUrl: './assets/to-user.jpg',
      toUserId: '140000198202211138',
      toUserName: 'Luff',
      userAvatar: '../../../../assets/icon/favicon.png',
      time: 1491034800000,
      message: 'Right, it totally blew my mind. They have other great apps and designs too !',
      status: 'success'
    },
    {
      messageId: '3',
      userId: '140000198202211138',
      userName: 'Luff',
      userImgUrl: './assets/user.jpg',
      toUserId: '210000198410281948',
      toUserName: 'Hancock',
      userAvatar: '../../../../assets/icon/favicon.png',
      time: 1491034920000,
      message: 'And it is free ?',
      status: 'success'
    },
    {
      messageId: '4',
      userId: '210000198410281948',
      userName: 'Hancock',
      userImgUrl: './assets/to-user.jpg',
      toUserId: '140000198202211138',
      toUserName: 'Luff',
      userAvatar: '../../../../assets/icon/favicon.png',
      time: 1491036720000,
      message: 'Yes, totally free. Beat that ! ',
      status: 'success'
    },
    {
      messageId: '5',
      userId: '210000198410281948',
      userName: 'Hancock',
      userImgUrl: './assets/to-user.jpg',
      toUserId: '140000198202211138',
      toUserName: 'Luff',
      userAvatar: '../../../../assets/icon/favicon.png',
      time: 1491108720000,
      message: 'Wow, that\'s so cool. Hats off to the developers. This is gooood stuff',
      status: 'success'
    },
    {
      messageId: '6',
      userId: '140000198202211138',
      userName: 'Luff',
      userImgUrl: './assets/user.jpg',
      toUserId: '210000198410281948',
      toUserName: 'Hancock',
      userAvatar: '../../../../assets/icon/favicon.png',
      time: 1491231120000,
      message: 'Check out their other designs.',
      status: 'success'
    }
  ];

  constructor() {
    this.msgList = [
      {
        userId: this.User,
        userName: this.User,
        userAvatar: '../../../../assets/icon/favicon.png',
        time: '12:01 pm',
        message: 'Hey, that\'s an awesome chat UI',
        upertext: 'Hello',
        status: 'success'
      },
      {
        userId: this.toUser,
        userName: this.toUser,
        userAvatar: '../../../../assets/icon/favicon.png',
        time: '12:01 pm',
        message: 'Right, it totally blew my mind. They have other great apps and designs too!',
        upertext: 'Hii',
        status: 'success'
      },
      {
        userId: this.User,
        userName: this.User,
        userAvatar: '../../../../assets/icon/favicon.png',
        time: '12:01 pm',
        message: 'And it is free ?',
        upertext: 'How r u ',
        status: 'success'
      },
      {
        userId: this.toUser,
        userName: this.toUser,
        userAvatar: '../../../../assets/icon/favicon.png',
        time: '12:01 pm',
        message: 'Yes, totally free. Beat that !',
        upertext: 'good',
        status: 'success'
      },
      {
        userId: this.User,
        userName: this.User,
        userAvatar: '../../../../assets/icon/favicon.png',
        time: '12:01 pm',
        message: 'Wow, that\'s so cool. Hats off to the developers. This is gooood stuff',
        upertext: 'How r u ',
        status: 'success'
      },
      {
        userId: this.toUser,
        userName: this.toUser,
        userAvatar: '../../../../assets/icon/favicon.png',
        time: '12:01 pm',
        message: 'Check out their other designs.',
        upertext: 'good',
        status: 'success'
      },
      {
        userId: this.User,
        userName: this.User,
        userAvatar: '../../../../assets/icon/favicon.png',
        time: '12:01 pm',
        message: 'Have you seen their other apps ? They have a collection of ready-made apps for developers. This makes my life so easy. I love it! ',
        upertext: 'How r u ',
        status: 'success'
      },
      {
        userId: this.toUser,
        userName: this.toUser,
        userAvatar: '../../../../assets/icon/favicon.png',
        time: '12:01 pm',
        message: 'Well, good things come in small package after all',
        upertext: 'good',
        status: 'pending'
      },
    ];

  }

  ngOnInit() {
  }

  scrollToBottom() {
    this.content.scrollToBottom(100);
  }

  

  ionViewDidEnter() {
    console.log('scrollBottom');
    setTimeout(() => {
      this.scrollToBottom();
    }, 500);
    console.log('scrollBottom2');
  }

  logScrollStart() {
    console.log('logScrollStart');
    document.getElementById('chat-parent');
  }

  logScrolling(event) {
    console.log('event', event);
  }

  sendMsg() {
    let otherUser;
    if (this.count === 0) {
      otherUser = this.arr[0].message;
      this.count++;
    } else if (this.count === this.arr.length) {
      this.count = 0;
      otherUser = this.arr[this.count].message;
    } else {
      otherUser = this.arr[this.count].message;
      this.count++;
    }

    this.msgList.push({
      userId: this.User,
      userName: this.User,
      userAvatar: '../../../../assets/icon/favicon.png',
      time: '12:01 pm',
      message: this.inpText,
      upertext: this.inpText,
      status: 'success'
    });
    this.msgList.push({
      userId: this.toUser,
      userName: this.toUser,
      userAvatar: '../../../../assets/icon/favicon.png',
      time: '12:01 pm',
      message: otherUser,
      upertext: otherUser,
      status: 'success'
    });
    this.inpText = '';
    console.log('scrollBottom');
    setTimeout(() => {
      this.scrollToBottom();
    }, 10);
  }

}
