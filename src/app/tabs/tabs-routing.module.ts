import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { CoursDetailModule } from '../mes-cours/cours-detail/cours-detail.module';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../profile/profile.module').then(m => m.ProfilePageModule)
          },
          {
            path: 'modify',
            loadChildren: () =>
              import('../profile/modify-profile/modify-profile.module').then(m => m.ModifyProfileModule)
          }
        ]
      },
      {
        path: 'mes-cours',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../mes-cours/mes-cours.module').then(m => m.MesCoursPageModule)
          },
          {
            path: 'detail/:cours',
            loadChildren: () =>
              import('../mes-cours/cours-detail/cours-detail.module').then(m => CoursDetailModule)
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab3/tab3.module').then(m => m.Tab3PageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/mes-cours',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/mes-cours',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
